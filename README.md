GNS3 CLI
========

gns3-cli provides a text base interface for GNS3 users.

This tools is not meant to replace the GUI for project creation (creating box, links or any graphical annotations) but
more for the day to day use of an already created project.

It currently features:

### Projects
```
[myGNS3Server]>
Help:
          close - Close a project
         export - [project_name] [download folder - default to current dir] - Export a project
         import - [archive name] - Import a project
           list - List projects
        project - Enter project context
[myGNS3Server]> list
Project name           Status    Id
---------------------  --------  ------------------------------------
lab1                   opened    615ba01e-e3bf-464e-ab29-2166f955e54f
lab2                   closed    51cd362e-7a8f-4668-b314-4ebfc3dd1c08
lab3                   closed    200a8bed-28c8-4fa2-8eb3-204b7b32b1a6
lab4                   closed    7660f059-5a75-4aeb-aa1a-4f0096a4036e
[myGNS3Server]> project lab3
[myGNS3Server](lab3)>
```

### Nodes
```
[myGNS3Server](lab3)>
Help:
        connect - Connect to a node. Support multiple node with a regex.
           list - List nodes. Use keyword detail for links information
          start - Start to a node. Support multiple node with a regex.
           stop - Stop to a node. Support multiple node with a regex.
[myGNS3Server](lab3)> list
Node name    Status    Console type      Console port
-----------  --------  --------------  --------------
NAT-1        started
switch_1     started   telnet                    5002
vm_1         started   telnet                    5003
[myGNS3Server](lab3)> connect all
<Opens new terminal with a tmux windows split between switch_1 and switch_2>
```


with some cool features:

* Auto-completion for all commands and most commands parameters
* Auto opening / closing of projects (configurable for auto-close)
* Tmux support for multiple connexions inside a single terminal
* Node connexion blacklisting
* SSH tunnelling for securing telnet serial port connexion


## Installation

This CLI has been tested on macOS High Sierra and Ubuntu 16.04 xfce4. It should be compatible with other linux
distributions / graphical environments with some tweaking.

Requirements:
* python3.5

```
virtualenv -p python3.5 .venv
source .venv
pip install -r requirements.txt

./gns3cli
```


## Roadmap 
There is no ETA nor particular order for the roadmap.

* Improve in-cli help
* ~~Import/Export support~~
* Import/Export CLOUD & NAT with nodes
* Packet capture support
* Link filters support
* Create an installer to help tweak the initial configuration in a more user-friendly way
* Improve installation documentation
* Package on pypi
* Add gif ttyrec in README for a better demo
* Support tmux session opening replacing the current terminal
* Support tmux session opening alongside the cli if it has been opened within a tmux session
* Add optional debug logs
* Support some configuration items via cli invocation arguments
* Handles multiples arguments (with auto-completion) for most commands instead of using a regex
* Get basic server info
