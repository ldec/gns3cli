import configparser

from ishell.console import Console

from objects.projects import (
    ListProjects,
    Project,
    CloseProject,
    ExportProject,
    ImportProject,
)

config = configparser.ConfigParser()
config.read("config.ini")

server_config = config["SERVER"]
main_config = config["MAIN"]
terminal_config = config["TERMINAL"]

import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def main():
    """
    Initiate the main context.

    Config.
        server.url [compulsory]: GNS3 server url
        server.port [compulsory]: GNS3 server port
        server.user [optional]: GNS3 api username
        server.password [optional]: GNS3 api password

    Childs
        project project_name - Enter project project_name
        close project_name - Close project project_name
        export project_name - Export project project_name
        list - List available projects
    """
    console = Console("[%s]" % server_config["url"])

    project = Project("project", help="Enter project context", dynamic_args=True)
    list_project = ListProjects("list", help="List projects")
    export_project = ExportProject(
        "export",
        help="[project_name] [download folder - default to current dir] - Export a project",
        dynamic_args=True,
    )
    import_project = ImportProject(
        "import", help="[archive name] - Import a project", dynamic_args=True
    )
    close_project = CloseProject("close", help="Close a project", dynamic_args=True)

    console.addChild(project)
    console.addChild(close_project)
    console.addChild(list_project)
    console.addChild(import_project)
    console.addChild(export_project)

    console.loop()


if __name__ == "__main__":
    main()
