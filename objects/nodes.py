import configparser
import os
import platform
import re
import shlex
import subprocess

import requests
from ishell.command import Command
from tabulate import tabulate

from utils import create_basic_auth, timeout_lru_cache, http_error_to_str

config = configparser.ConfigParser()
config.read("config.ini")

server_config = config["SERVER"]
main_config = config["MAIN"]
terminal_config = config["TERMINAL"]
ssh_tunnel_config = config["SSH_TUNNEL"]

IS_MAC_OS = platform.system() == "Darwin"


@timeout_lru_cache()
def get_node_list_data(project_id):
    """
    Fetch project_id available node data

    :param project_id: Current project_id
    :type project_id: str
    """
    response = requests.get(
        "http://%s:%s/v2/projects/%s/nodes"
        % (server_config["url"], server_config["port"], project_id),
        auth=create_basic_auth(server_config["user"], server_config["password"]),
    )

    if response.status_code == requests.codes.ok:
        return response.json(), None
    else:
        return None, http_error_to_str(response)


@timeout_lru_cache()
def get_link_list_data(project_id):
    """
    Fetch project_id available link data

    :param project_id: Current project_id
    :type project_id: str
    """
    response = requests.get(
        "http://%s:%s/v2/projects/%s/links"
        % (server_config["url"], server_config["port"], project_id),
        auth=create_basic_auth(server_config["user"], server_config["password"]),
    )

    if response.status_code == requests.codes.ok:
        return response.json(), None
    else:
        return None, http_error_to_str(response)


def close_node(project_id, node_id):
    response = requests.post(
        "http://%s:%s/v2/projects/%s/nodes/%s/stop"
        % (server_config["url"], server_config["port"], project_id, node_id),
        auth=create_basic_auth(server_config["user"], server_config["password"]),
    )

    if (
        response.status_code == requests.codes.ok
        or response.status_code == requests.codes.created
    ):
        return response.json(), None
    else:
        return None, http_error_to_str(response)


def get_node_by_name_or_regex(project_id, project_name_or_regex, blacklist=None):
    """
    Get project_id node(s) matching project_name_or_regex

    :param project_id: Current project_id
    :type project_id: str
    :param project_name_or_regex: Node name or regex for node name matching
    :type project_name_or_regex: str
    :return: List of nodes
    :rtype: [Nodes]
    """
    if blacklist is None:
        blacklist = []

    node_data, error = get_node_list_data(project_id)
    if node_data is None:
        print(error)
        return []

    nodes_by_name = {}
    for node in node_data:
        nodes_by_name[node["name"]] = node

    nodes = []
    if project_name_or_regex in nodes_by_name:
        nodes.append(nodes_by_name[project_name_or_regex])
    else:
        # Search nodes matching the regex
        regex = re.compile(project_name_or_regex)
        for node_name, node in sorted(nodes_by_name.items()):
            if regex.search(node_name) and node_name not in blacklist:
                nodes.append(node)

    return nodes


def get_node_name_list(project_id):
    """
    Get project_id node list

    :param project_id: Current project_id
    :type project_id: str
    :return: List of node names
    :rtype: [str] or []
    """
    data, error = get_node_list_data(project_id)
    if data is None:
        print("Auto-completion unavailable - %s" % error)
        return []

    nodes_names = []
    for node in data:
        nodes_names.append(node["name"])
    return nodes_names


def get_linked_nodes(links, node_id, adapter_number, port_number):
    """
    Get links attached to node_id

    :param links: Links data
    :type links: [Links]
    :param node_id: Node id
    :type node_id: str
    :param adapter_number: Adapter number
    :type adapter_number: int
    :param port_number: Port number
    :type port_number: int
    :return: Link between nodes
    :rtype: link
    """
    for link in links:
        for node in link["nodes"]:
            if node["node_id"] != node_id:
                continue
            if node["adapter_number"] != adapter_number:
                continue
            if node["port_number"] != port_number:
                continue
            return link


def get_node_list_from_line(line, project_id, blacklist=None):
    line = line.split()
    if len(line) < 2:
        print("\% Please provide a node name or a matching regex")
        return None

    node_name_or_regex = line[-1]

    if node_name_or_regex == "all":
        node_name_or_regex = ".*"

    return get_node_by_name_or_regex(
        project_id, node_name_or_regex, blacklist=blacklist
    )


class ListNodes(Command):
    def __init__(self, *args, **kwargs):
        self.project_id = kwargs.pop("project_id")
        super(ListNodes, self).__init__(*args, **kwargs)

    def args(self):
        return ["detail"]

    def run(self, line):
        """
        List nodes command

        Default behaviour:
            List current project nodes

        Arguments:
            - detail [optional]
                List current project nodes and links between nodes.

        Parent: Project
        """
        node_data, error = get_node_list_data(self.project_id)
        if node_data is None:
            print(error)
            return

        nodes = {}
        for node in node_data:
            nodes[node["name"]] = node

        is_detail = False
        if line.split()[-1] == "detail":
            is_detail = True
            links_data, error = get_link_list_data(self.project_id)
            if links_data is None:
                print(error)

        if not is_detail:
            devices = [("Node name", "Status", "Console type", "Console port")]
            for node_name, node in sorted(nodes.items()):
                devices.append(
                    (node_name, node["status"], node["console_type"], node["console"])
                )

            print(tabulate(devices, headers="firstrow"))

        else:
            devices = {}
            devices_by_name = {}
            for device in node_data:
                device_uuid = device["node_id"]
                device_name = device["name"]

                devices[device_uuid] = device
                devices_by_name[device_name] = device

            separator_length = None
            for name in sorted(devices_by_name.keys()):
                if separator_length is not None:
                    print("=" * separator_length)
                    print()

                device = devices_by_name[name]

                print(
                    "%s %s %s\n"
                    % (device["name"], device["console"], device["status"].upper())
                )

                links = [("Interface", "Neighbor name", "Neighbor interface")]

                for port in device["ports"]:
                    link = get_linked_nodes(
                        links_data,
                        device["node_id"],
                        port["adapter_number"],
                        port["port_number"],
                    )

                    # no link associated to this port
                    if link is None:
                        # print("    {0}".format(port["name"]))
                        links.append((port["name"], "", ""))
                        continue

                    # find the other node associated to this link
                    other_node = None
                    for node in link["nodes"]:
                        if node["node_id"] == device["node_id"]:
                            continue

                        other_node = node
                        break

                    # node not found
                    if other_node is None:
                        links.append((port["name"], "?", "?"))
                        continue

                    # node is not in devices
                    if other_node["node_id"] not in devices:
                        links.append((port["name"], other_node["name"], ""))
                        continue

                    distant_device = devices[other_node["node_id"]]

                    # find the port on the other node
                    device_port = None
                    for distant_port in distant_device["ports"]:
                        if (
                            distant_port["adapter_number"]
                            != other_node["adapter_number"]
                        ):
                            continue

                        if distant_port["port_number"] != other_node["port_number"]:
                            continue

                        device_port = distant_port
                        break

                    if device_port is not None:
                        links.append(
                            (port["name"], distant_device["name"], device_port["name"])
                        )
                    else:
                        links.append((port["name"], distant_device["name"], "?"))
                to_print = tabulate(links, headers="firstrow")
                print(to_print)
                separator_length = len(max(to_print.splitlines(), key=len))


class StartNode(Command):
    def __init__(self, *args, **kwargs):
        self.project_id = kwargs.pop("project_id")
        super(StartNode, self).__init__(*args, **kwargs)

    def args(self):
        """
        Auto completion with project names and all
        """
        data = get_node_name_list(self.project_id)
        if data is None:
            return []
        return ["all"] + data

    def run(self, line):
        """
        Start node(s) command

        Arguments:
            - node_name_or_regex | all [compulsory]
                Start node(s) matching name or regex
                All starts every nodes.

        Parent: Project
        """
        nodes_to_start = get_node_list_from_line(line, self.project_id)
        if nodes_to_start is None:
            return None

        for node_to_start in nodes_to_start:
            if node_to_start["status"] == "started":
                print("%% Node %s already started" % node_to_start["name"])
                continue

            response = requests.post(
                "http://%s:%s/v2/projects/%s/nodes/%s/start"
                % (
                    server_config["url"],
                    server_config["port"],
                    self.project_id,
                    node_to_start["node_id"],
                ),
                auth=create_basic_auth(
                    server_config["user"], server_config["password"]
                ),
            )

            if (
                response.status_code == requests.codes.ok
                or response.status_code == requests.codes.created
            ):
                print("Node %s started" % node_to_start["name"])
            else:
                print(
                    "Node %s could not be stared: %s"
                    % (node_to_start["name"], response.text)
                )

        get_node_list_data.cache_clear()


class StopNode(Command):
    def __init__(self, *args, **kwargs):
        self.project_id = kwargs.pop("project_id")
        super(StopNode, self).__init__(*args, **kwargs)

    def args(self):
        """
        Auto completion with project names and all
        """
        data = get_node_name_list(self.project_id)
        if data is None:
            return []
        return ["all"] + data

    def run(self, line):
        """
        Stop node(s) command

        Arguments:
            - node_name_or_regex or all [compulsory]
                Stop node(s) matching name or regex.
                All stops every nodes.

        Parent: Project
        """
        nodes_to_stop = get_node_list_from_line(line, self.project_id)
        if nodes_to_stop is None:
            return None

        for node_to_stop in nodes_to_stop:

            if node_to_stop["status"] == "stopped":
                print("Node %s already stopped" % node_to_stop["name"])
                continue

            closed, error = close_node(self.project_id, node_to_stop["node_id"])

            if closed:
                print("Node %s stopped" % node_to_stop["name"])
            else:
                print(
                    "Node %s could not be stopped: %s" % (node_to_stop["name"], error)
                )

        get_node_list_data.cache_clear()


class ConnectNode(Command):
    def __init__(self, *args, **kwargs):
        self.project_id = kwargs.pop("project_id")
        self.project_name = kwargs.pop("project_name")
        super(ConnectNode, self).__init__(*args, **kwargs)

    def args(self):
        """
        Auto completion with project names and all
        """
        data = get_node_name_list(self.project_id)
        if data is None:
            return []
        return ["all"] + data

    def run(self, line):
        """
        Connect node(s) command

        Arguments:
            - node_name_or_regex | all [compulsory]
                Connect node(s) matching name or regex.
                All connects every nodes.

        Side-effect: Open a new terminal with a tmux sessions with tiled panels.

        Config.
            terminal.executable [compulsory]: Terminal executable
            terminal.executable_arguments [optional]: Customize terminal options.
                Warning: Last option must enable command execution (like -c with bash) if the terminal needs it.

        Parent: Project
        """

        # Check if the current project has connexion blacklisted nodes.
        blacklist = None
        if config.has_section(self.project_name) and config.has_option(
            self.project_name, "connect_blacklist"
        ):
            blacklist = config[self.project_name]["connect_blacklist"].split(",")

        nodes_to_connect = get_node_list_from_line(
            line, self.project_id, blacklist=blacklist
        )
        if nodes_to_connect is None:
            return None

        if not nodes_to_connect:
            print("Node doesn't exit or regex doesn't match any available nodes")
        else:
            xpanes_command = None
            other_commands = []
            for node_to_connect in nodes_to_connect:
                if node_to_connect["status"] != "started":
                    print(
                        "Could not connect to %s, node is currently %s"
                        % (node_to_connect["name"], node_to_connect["status"])
                    )
                    continue

                if node_to_connect["console_type"] == "telnet":
                    telnet_command = "telnet %s %s" % (
                        server_config["url"],
                        node_to_connect["console"],
                    )

                    if ssh_tunnel_config["active"] == "true":
                        telnet_raw_command = (
                            "ssh %(bastion_username)s@%(bastion_host)s -t %(server_username)s@%(server_host)s -- "
                            "telnet 127.0.0.1 %(console_port)s"
                        )
                        if IS_MAC_OS:
                            # TODO: Make bastion usage configurable
                            telnet_raw_command = (
                                "ssh %(bastion_username)s@%(bastion_host)s -t %(server_username)s@%(server_host)s -t -- "
                                '\\\\\\"telnet 127.0.0.1 %(console_port)s\\\\\\"'
                            )

                        telnet_command = telnet_raw_command % {
                            "bastion_username": ssh_tunnel_config["bastion_username"],
                            "bastion_host": ssh_tunnel_config["bastion_host"],
                            "server_username": ssh_tunnel_config["server_username"],
                            "server_host": server_config["url"],
                            "console_port": node_to_connect["console"],
                        }

                    if xpanes_command is None:
                        xpanes_command = "xpanes -d -t -e"

                    xpanes_node_command = "%s 'set_title %s ; %s'"
                    if IS_MAC_OS:
                        xpanes_node_command = '%s \\"set_title %s ; %s\\"'

                    xpanes_command = xpanes_node_command % (
                        xpanes_command,
                        node_to_connect["name"],
                        telnet_command,
                    )
                elif node_to_connect["console_type"] == "vnc":
                    other_commands.append(
                        "vncviewer %s:%s"
                        % (server_config["url"], node_to_connect["console"])
                    )

            FNULL = open(os.devnull, "w")
            # Launch xpanes commands separately than other commands.
            if xpanes_command is not None:
                # Create new terminal command
                # Behaviour is different on Mac OS.
                new_term_base_command = '%s %s bash -ci "%s"'
                if IS_MAC_OS:
                    new_term_base_command = "%s %s \"bash -ci '%s'\""

                new_term_command = new_term_base_command % (
                    terminal_config["executable"],
                    terminal_config["executable_arguments"],
                    xpanes_command,
                )

                subprocess.Popen(
                    shlex.split(new_term_command), stdout=FNULL, stderr=FNULL
                )

            # Launch other commands "as is"
            if other_commands:
                for command in other_commands:
                    print(command)
                    subprocess.Popen(shlex.split(command), stdout=FNULL, stderr=FNULL)
