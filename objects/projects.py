import configparser
import glob
import re
from os import getcwd
from os.path import join, isfile
from uuid import uuid4

import requests
from ishell.command import Command
from tabulate import tabulate

from objects.nodes import (
    ConnectNode,
    ListNodes,
    StartNode,
    StopNode,
    get_node_list_data,
    close_node,
)
from utils import (
    create_basic_auth,
    timeout_lru_cache,
    http_error_to_str,
    ask_user,
    download_file,
    upload_file,
)

config = configparser.ConfigParser()
config.read("config.ini")

server_config = config["SERVER"]
main_config = config["MAIN"]
terminal_config = config["TERMINAL"]

GNS3_PROJECT_EXT = re.compile("\.gns3p(roject)?$")

# Some GNS3 nodes are managed by GNS3 and cannot be stopped manually.
UNSTOPPABLE_NODES = [
    "ethernet_switch",
    "ethernet_hub",
    "frame_relay_switch",
    "cloud",
    "nat",
]


@timeout_lru_cache()
def get_project_list_data():
    """
    Fetch the server available project data
    """
    response = requests.get(
        "http://%s:%s/v2/projects" % (server_config["url"], server_config["port"]),
        auth=create_basic_auth(server_config["user"], server_config["password"]),
    )

    if response.status_code == requests.codes.ok:
        return response.json(), None
    else:
        return None, http_error_to_str(response)


def get_project_names():
    """
    Get projects name for auto-completion
    """
    data, error = get_project_list_data()
    if data is None:
        print("\nAuto-completion unavailable - %s" % error)
        return None

    project_names = []
    for project in data:
        project_names.append(project["name"])
    return project_names


def close_project(project_id):
    response = requests.post(
        "http://%s:%s/v2/projects/%s/close"
        % (server_config["url"], server_config["port"], project_id),
        auth=create_basic_auth(
            server_config["url"], password=server_config["password"]
        ),
    )

    get_project_list_data.clear_cache()
    if response.status_code != requests.codes.no_content:
        return response.json(), None
    else:
        return None, http_error_to_str(response)


class Project(Command):
    project_id = None
    project_name = None

    def args(self):
        """
        Auto completion with project names
        """
        return get_project_names()

    def run(self, line):
        """
        Enter project $1 context.

        Side-effect: Opens the project $1 on GNS3

        Child:
            list [detail]: List available nodes, use detail argument for links list.
            start [node_name | node_regex]: Start node node_name or all nodes matching node_regex
            stop [node_name | node_regex]: Stop node node_name or all nodes matching node_regex
            connect [node_name | node_regex]: Open telnet connexions for node_name or all nodes matching node_regex

        Exit side-effect: Exit the project on GNS3 if project_auto_close is set to true

        Config:
            main.project_auto_close [compulsory]: Auto close project on context exit.
        """

        line = line.split()
        if len(line) < 2:
            print("\% Please provide a project name")
            return

        project_name = line[-1]
        self.prompt = "[%s](%s)" % (server_config["url"], project_name)

        data, error = get_project_list_data()
        if data is None:
            print(error)
            return

        for project in data:
            if project["name"] == project_name:
                project_data = project
                break
        else:
            print(
                "%% Project %s not found on server %s"
                % (project_name, server_config["url"])
            )
            return

        self.project_id = project_data.get("project_id")
        self.project_name = project_data.get("name")

        # Open the project
        requests.post(
            "http://%s:%s/v2/projects/%s/open"
            % (server_config["url"], server_config["port"], self.project_id),
            auth=create_basic_auth(
                server_config["url"], password=server_config["password"]
            ),
        )
        get_project_list_data.cache_clear()

        list_nodes = ListNodes(
            "list",
            project_id=self.project_id,
            help="List nodes. Use keyword detail for links information",
            dynamic_args=True,
        )

        start_node = StartNode(
            "start",
            project_id=self.project_id,
            help="Start to a node. Support multiple node with a regex.",
            dynamic_args=True,
        )

        stop_node = StopNode(
            "stop",
            project_id=self.project_id,
            help="Stop a node. Support multiple node with a regex.",
            dynamic_args=True,
        )

        connect_node = ConnectNode(
            "connect",
            project_id=self.project_id,
            project_name=self.project_name,
            help="Connect a node. Support multiple node with a regex.",
            dynamic_args=True,
        )

        self.addChild(list_nodes)
        self.addChild(connect_node)
        self.addChild(start_node)
        self.addChild(stop_node)

        self.loop()

    def on_loop_exit(self):
        # Close the project if project_auto_close is set to True
        if main_config["project_auto_close"].lower() == "true":
            _, error = close_project(self.project_id)
            if error is not None:
                print(error)


class CloseProject(Command):
    def args(self):
        """
        Auto completion with project names
        """
        return get_project_names()

    def run(self, line):
        line = line.split()
        if len(line) < 2:
            print("\% Please provide a project name")
            return

        project_name = line[-1]
        data, error = get_project_list_data()

        if data is None:
            print(error)
            return

        for project in data:
            if project["name"] == project_name:
                project_data = project
                break
        else:
            print(
                "%% Project %s not found on server %s"
                % (project_name, server_config["url"])
            )
            return

        _, error = close_project(project_data["project_id"])
        if error is not None:
            print(error)


class ImportProject(Command):
    def args(self):
        """
        Auto completion with current folder zip files
        """
        return [x for x in glob.glob("*") if GNS3_PROJECT_EXT.search(x)]

    def run(self, line):
        line = line.split()
        if len(line) < 2:
            print("\% Please provide an archive filename")
            return

        project_archive_filename = line[-1]
        project_archive_path = join(getcwd(), project_archive_filename)

        if not isfile(project_archive_path):
            print("%% File %s does not exists" % project_archive_path)
            return

        project_name = GNS3_PROJECT_EXT.sub("", project_archive_filename)

        response = ask_user(
            "Project will be named %s, enter to accept or write new name"
            % project_name,
            return_answer=True,
        )
        if response != "":
            project_name = response

        response, error = upload_file(
            "http://%s:%s/v2/projects/%s/import"
            % (server_config["url"], server_config["port"], uuid4()),
            project_archive_path,
            params={"name": project_name},
            auth=create_basic_auth(server_config["user"], server_config["password"]),
        )

        if error is not None:
            print("Import failed - %s " % error)
            return

        print("Project %s has been imported" % response["name"])


class ExportProject(Command):
    def args(self):
        """
        Auto completion with project names
        """
        return get_project_names()

    def run(self, line):
        """
        Export the project

        Parent: Main
        """
        line = line.split()
        if len(line) < 2:
            print(
                "%% Please provide a project name and optionally a download folder path"
            )
            return

        # If the second argument is not provided, use the current directory
        if len(line) == 2:
            project_name = line[-1]
            archive_folder_path = getcwd()
        else:
            project_name = line[-2]
            archive_folder_path = line[-1]

        data, error = get_project_list_data()

        if data is None:
            print(error)
            return

        for project in data:
            if project["name"] == project_name:
                project_data = project
                break
        else:
            print(
                "%% Project %s not found on server %s"
                % (project_name, server_config["url"])
            )
            return

        archive_download_path = join(
            archive_folder_path, "%s.gns3p" % project_data["name"]
        )

        if isfile(archive_download_path):
            overwrite = ask_user(
                "File %s already exists, do you want to overwrite it"
                % archive_download_path
            )
            if not overwrite:
                print("Aborting export")
                return

        nodes_data, error = get_node_list_data(project_data["project_id"])
        if error is not None:
            print(error)
            return

        # Projects cannot be exported until all nodes are closed
        nodes_to_shut = []
        for node in nodes_data:
            if (
                node["status"] == "started"
                and node["node_type"] not in UNSTOPPABLE_NODES
            ):
                nodes_to_shut.append((node["name"], node["node_id"]))

        if nodes_to_shut:
            plural = len(nodes_to_shut) > 1
            response = ask_user(
                "Node%s %s needs to be stopped, stop %s ?"
                % (
                    "s" if plural else "",
                    ", ".join([node_tuple[0] for node_tuple in nodes_to_shut]),
                    "them" if plural else "it",
                )
            )

            if response:
                failed = False
                for _, node_to_shut in nodes_to_shut:
                    pass
                    closed, error = close_node(project_data["project_id"], node_to_shut)
                    if error is not None:
                        print(error)
                        failed = True
                if failed:
                    print("Node closing has failed")
                    get_node_list_data.cache_clear()
                    return
                print("All nodes have been closed")
                get_node_list_data.cache_clear()
            else:
                print("Aborting export")
                return

        # Does the user wants to include full images in the archive ?
        print(
            "Would you like to include any base image?\n"
            "The project will not require additional images to run on another host, however "
            "the resulting file will be much bigger."
        )
        include_images = int(
            ask_user(
                "It is your responsibility to check if you have the right "
                "to distribute the image(s) as part of the project"
            )
        )

        include_readme = ask_user("Do you want to include a README")
        if include_readme:
            readme_content = ask_user(
                "Please enter the README content. Use Ctrl + D to save", multi_line=True
            )
            # Update the readme on the server before exporting
            response = requests.post(
                "http://%s:%s/v2/projects/%s/files/%s"
                % (
                    server_config["url"],
                    server_config["port"],
                    project_data["project_id"],
                    "README.txt",
                ),
                data=readme_content,
                headers={"content-type": "text/plain"},
                auth=create_basic_auth(
                    server_config["user"], password=server_config["password"]
                ),
            )

            if response.status_code != requests.codes.ok:
                print(http_error_to_str(response))
            else:
                print("Readme saved")

        print("Downloading %s" % archive_download_path)

        _, error = download_file(
            "http://%s:%s/v2/projects/%s/export"
            % (server_config["url"], server_config["port"], project_data["project_id"]),
            archive_download_path,
            params={"include_images": include_images},
            auth=create_basic_auth(
                server_config["user"], password=server_config["password"]
            ),
        )
        if error:
            print("Export fo project %s failed - %s" % (project_name, error))
        else:
            print("Download finished")


class ListProjects(Command):
    def run(self, line):
        """
        List project command

        Parent: Main
        """
        data, error = get_project_list_data()

        if data is None:
            print(error)
            return

        projects = [("Project name", "Status", "Id")]
        for project in data:
            projects.append((project["name"], project["status"], project["project_id"]))

        print(tabulate(projects, headers="firstrow"))
