import json
import sys
from concurrent.futures import ProcessPoolExecutor
from functools import lru_cache
from time import time, sleep

import requests
from requests.auth import HTTPBasicAuth

CACHE_TIMEOUT = 30
CACHE_SIZE = 128

BLINKING_MESSAGE_DOT_NUMBER = 3


def timeout_lru_cache(maxsize=CACHE_SIZE, timeout=CACHE_TIMEOUT):
    """
    Implement a lru cache with a timeout

    :param maxsize: Max lru cache size
    :type maxsize: int
    :param timeout: Max cache item age
    :type timeout: int
    :return:
    :rtype:
    """

    def decorator(f):
        @lru_cache(maxsize=maxsize)
        def cached_function(_cache_timestamp, *args, **kwargs):
            return f(*args, **kwargs)

        def new_function(*args, **kwargs):
            timestamp = int(time() / timeout)
            return cached_function(timestamp, *args, **kwargs)

        new_function.cache_info = cached_function.cache_info
        new_function.cache_clear = cached_function.cache_clear
        return new_function

    return decorator


def http_error_to_str(response):
    """
    Format response error

    :param response: requests response
    :return: Formatted error
    """
    data = {"status_code": response.status_code}
    try:
        json_error = response.json()
        data["context"] = json_error.get("message")
    except json.decoder.JSONDecodeError:
        data["context"] = response.reason

    return "Error %(status_code)s: %(context)s" % data


def download_file(url, file_path, auth=None, params=None):
    """
    Download a file

    If available, display a progress bar, else display a blinking
    message.

    :param url: File url
    :param file_path: File path
    :param auth: Get auth
    :param params: Get params
    :return: success, error
    """
    with open(file_path, "wb") as f:
        response = requests.get(url, stream=True, auth=auth, params=params)

        if response.status_code != requests.codes.ok:
            return False, http_error_to_str(response)

        total_length = response.headers.get("content-length")

        if total_length is None:
            # no content length header, display a basic "blinking" message
            start = int(time())
            for data in response.iter_content(chunk_size=4096):
                f.write(data)
                # Add a new dot each second, until 3 dots, then restart to 0 dot.
                blink_message("Downloading", start, BLINKING_MESSAGE_DOT_NUMBER)
            print()
        else:
            # Display a progress bar
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                # Each = correspond to 2%
                done = int(50 * dl / total_length)
                sys.stdout.write(
                    "\r[%s%s] - %s %%" % ("=" * done, " " * (50 - done), 2 * done)
                )
                sys.stdout.flush()
            print()

        return True, None


def upload_file(url, path, params=None, auth=None, message="Uploading"):
    """
    Upload a file

    Request does not support upload streaming, upload in a thread and
    print a blinking message in the meanwhile.

    :param url: Upload url
    :param path: File path
    :param params: POST params
    :param auth: POST auth
    :param message: Waiting message
    :return: json response, data
    """

    start = int(time())
    pool = ProcessPoolExecutor(1)
    future = pool.submit(_upload_file, url, path, params=params, auth=auth)

    while not future.done():
        blink_message(message, start, BLINKING_MESSAGE_DOT_NUMBER)

    print()
    data, error = future.result()

    return data, error


def _upload_file(url, path, params=None, auth=None):
    """
    Upload a file

    :param url: Upload url
    :param path: File path
    :param params: POST params
    :param user: POST user
    :param password: POST password
    :param message: Waiting message
    :return: json response, data
    """
    with open(path, "rb") as data:
        response = requests.post(
            url,
            data=data,
            params=params,
            headers={"Content-Type": "application/octet-stream"},
            auth=auth,
        )
    if response.status_code != requests.codes.created:
        return None, http_error_to_str(response)
    return response.json(), None


def blink_message(message, start, dots):
    """
    Display a blinking message.

    Only dots after the initial message will blink, with a limit of
    dots dots.
    :param message: Initial message
    :param start: Start time
    :param dots: Max number of dots
    """
    modulus = (int(time()) - start) % dots + 1
    sys.stdout.write("\r%s%s" % (message, "." * modulus))
    sys.stdout.flush()
    sleep(0.1)


def ask_user(question, default=True, return_answer=False, multi_line=False):
    """
    Ask the user for input

    If both return_answer and multi_line are False, the answer is considered
    to be a boolean, default will be used for the default choice.

    If return_answer or multi_line are set, default is ignored.

    If multi_line is set, return_answer is implied

    :param question: Question prompt
    :param default: Default bool choice
    :param return_answer: Return a str answer
    :param multi_line: Return a multi line answer
    :return: answer (bool or str)
    """
    if multi_line:
        return_answer = True

    yes = "y"
    no = "n"
    choice = ""
    if default and not return_answer:
        yes = yes.upper()
        choice = "[%s/%s]" % (no, yes)

    if not multi_line:
        response = input("%s %s: " % (question, choice))
    else:
        print("%s: " % question)
        response_split = []
        while True:
            try:
                line = input()
            except EOFError:
                break
            response_split.append(line)
        response = "\n".join(response_split)

    if not response and not return_answer:
        return default
    elif not return_answer:
        return yes in response
    elif return_answer:
        return response


def create_basic_auth(user, password=None):
    """
    Create a basic auth object
    :param user: User
    :param password: Password
    :return: auth
    """
    if user is not None:
        return HTTPBasicAuth(user, password)
    else:
        return None
